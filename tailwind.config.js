module.exports = {
  content: [
    './documentation/*.{html,md}',
    './documentation/**/*.{html,md}',
    './src/*.pcss',
    './src/**/*.pcss'
  ],
  theme: {
    extend: {
      ringWidth: {
        '3': '3px',
        '5': '5px',
      },
      
      colors: {
        'gnome-blue': {
          100: '#CDE1F8',
          200: '#B8D4F5',
          300: '#99C1F1',
          400: '#62A0EA',
          500: '#3584E4',
          600: '#337FDC',
          700: '#1C71D8',
          800: '#1A5FB4',
          900: '#1756A2',
        },
        gray: {
          100: '#F6F5F4',
          200: '#DEDDDA',
          300: '#C0BFBC',
          400: '#9A9996',
          500: '#77767B',
          600: '#5E5C64',
          700: '#3D3846',
          800: '#241F31',
          900: '#1D1927',
        },
        red: {
          100: '#F87F72',
          200: '#F77162',
          300: '#F66151',
          400: '#EF474F',
          500: '#ED333B',
          600: '#E01B24',
          700: '#C01C28',
          800: '#A51D2D',
          900: '#951A29',
        },
        orange: {
          100: '#FFD197',
          200: '#FFCB8A',
          300: '#FFBE6F',
          400: '#FFA348',
          500: '#FF7800',
          600: '#E66100',
          700: '#C64600',
          800: '#B23F00',
          900: '#8E3200',
        },
        yellow: {
          100: '#FAF389',
          200: '#F9F06B',
          300: '#F8E45C',
          400: '#F6D32D',
          500: '#F5C211',
          600: '#E5A50A',
          700: '#F5C211',
          800: '#B98608',
          900: '#A77907',
        },
        green: {
          100: '#C0F7CC',
          200: '#AEF4BD',
          300: '#8FF0A4',
          400: '#57E389',
          500: '#33D17A',
          600: '#2EC27E',
          700: '#26A269',
          800: '#1E8254',
          900: '#186843',
        },
        brown: {
          100: '#D2B39A',
          200: '#CDAB8F',
          300: '#C49C7B',
          400: '#B5835A',
          500: '#986A44',
          600: '#865E3C',
          700: '#63452C',
          800: '#503824',
          900: '#402D1D',
        },
        purple: {
          100: '#EBBDEB',
          200: '#E6AAE6',
          300: '#DC8ADD',
          400: '#C061CB',
          500: '#AD57B7',
          600: '#9141AC',
          700: '#813D9C',
          800: '#613583',
          900: '#4E2B6A',
        }
      }
    },

    fontFamily: {
      sans: [
        'Cantarell',
        'Source Sans Pro',
        'Droid Sans',
        'Ubuntu',
        'DejaVu Sans',
        'Arial',
        'sans-serif',
      ],
      mono: [
        'Monaco',
        'monospace',
        'Menlo',
      ],
    },

    fontSize: {
      'xs': '.75rem',   
      'sm': '.875rem',
      'base': '1.125rem',
      'lg': '1.4625rem',
      'xl': '2.925rem',
      '2xl': '3.15rem',
    },

    fontWeight: {
      light: '300',
      'regular': '400',
      bold: '600',
      extrabold: '800',
    },

    borderWidth: {
      DEFAULT: '1px',
      '0': '0',
      '2': '2px',
      '3': '3px',
      '4': '4px',
      '8': '8px',
    },
  },
  
  plugins: [require("@tailwindcss/forms")]
}
